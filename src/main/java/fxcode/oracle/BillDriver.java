package fxcode.oracle;


import javafx.beans.value.ObservableValue;
import javafx.beans.value.ChangeListener;

public class BillDriver {

    public static void main(String[] args) {

        Bill electricBill = new Bill();

        //events are controller with properties and bindings.

        electricBill.amountDueProperty().addListener(new ChangeListener(){
            @Override public void changed(ObservableValue o,Object oldVal,
                                          Object newVal){
                System.out.println("Electric bill has changed from " + oldVal + " to " + newVal);
            }
        });

        electricBill.setAmountDue(100.00);
        electricBill.setAmountDue(50.25);
        electricBill.setAmountDue(78.14);
        electricBill.setAmountDue(98.23);

    }
}