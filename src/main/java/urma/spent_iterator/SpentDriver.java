package urma.spent_iterator;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Stream;

/**
 * Created by Adam on 7/15/2015.
 */
public class SpentDriver {


    public static void main(String[] args) {
        List<Person> sample = Arrays.asList(
                new Person(280, "usa"),
                new Person(189, "china"),
                new Person(155, "russia"),
                new Person(125, "canada"),
                new Person(120, "china"),
                new Person(185, "russia"),
                new Person(155, "china"),
                new Person(195, "usa"),
                new Person(155, "russia"),
                new Person(255, "usa"),
                new Person(155, "usa"),
                new Person(200, "china"));


        System.out.println("use iterator &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&");
        Iterator<Person> iterator = sample.iterator();

        while(iterator.hasNext()){
            System.out.println(iterator.next());
        }


        System.out.println("the iterator is spent and will produce nothing &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&");
        //the iterator is spent and will produce nothing
        while(iterator.hasNext()){
            System.out.println(iterator.next());
        }



        System.out.println("using yet more Java8 streams &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&");
        //using java8 streams
       Stream<Person> personStream =  sample.parallelStream();


                personStream
                .filter( p -> p.getOrigin().equals("china") && p.getIq() > 150) //itermediate - returns Stream<Person>
                .sorted((a, b) -> a.getIq().compareTo(b.getIq())) //itermediate - returns Stream<Person>
                .map(p -> new Person(p.getIq(), p.getOrigin().toUpperCase()))//itermediate - returns Stream<Person>
                .forEach(p -> System.out.println("smart chinese person: " + p.toString())); //terminal - executes iteration

// Stream<Person> personStream2 =  sample.stream();
//        System.out.println("the stream is spent (will throw exception) &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&");
//             personStream
//                .filter(p -> p.getOrigin().equals("china") && p.getIq() > 150) //itermediate - returns Stream<Person>
//                .sorted((a, b) -> a.getIq().compareTo(b.getIq())) //itermediate - returns Stream<Person>
//                .map(p -> new Person(p.getIq(), p.getOrigin().toUpperCase())) //itermediate - returns Stream<Person>
//                .forEach(p -> System.out.println("smart chinese person: " + p.toString())); //terminal - executes iteration


    }


    //our POJO
    public static class Person {
        private int iq = 0;
        private String origin = "";

        public Person(int iq, String origin) {
            this.iq = iq;
            this.origin = origin;
        }

        public Integer getIq() {
            return iq;
        }

        public void setIq(Integer iq) {
            this.iq = iq;
        }

        public String getOrigin() {
            return origin;
        }

        public void setOrigin(String origin) {
            this.origin = origin;
        }

        public String toString() {
            return "Person{" +
                    "origin='" + origin + '\'' +
                    ", iq=" + iq +
                    '}';
        }
    }
}
