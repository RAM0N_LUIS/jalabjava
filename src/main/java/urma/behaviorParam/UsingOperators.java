package urma.behaviorParam;

import java.util.Date;
import java.util.function.BiFunction;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.UnaryOperator;

/**
 * Created by Adam on 6/23/2016.
 */
public class UsingOperators {

    public static void main(String[] args) {

        //unary opeator

        //operators are just short-hand for Functions.

        //these operators just extend function, so they just take a string and return a string
        UnaryOperator<String> unaryOp  = s -> s.toUpperCase();
        System.out.println(unaryOp.apply("adam"));

        //this is the same as above
        Function<String,String> upperCaseFullStyle =  s -> s.toUpperCase();
        System.out.println(upperCaseFullStyle.apply("adam"));

        //binary operator. This takes to Integers and returns an Integer
        BinaryOperator<Integer> adder = (n1, n2) -> n1 + n2;

        //the above is the same as
        BiFunction<Integer,Integer,Integer> adderFullStyle = (n1, n2) -> n1 + n2;

        System.out.println(adder.apply(3, 41));

        System.out.println(adderFullStyle.apply(3, 41));


        //another binary operator
        BinaryOperator<Date> dateBinaryOperatorSubtractor = (dat1, dat2) ->  new Date(dat1.getTime() - dat2.getTime());

        System.out.println(dateBinaryOperatorSubtractor.apply(new Date(), new Date()));

    }


}
