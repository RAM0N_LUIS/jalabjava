package urma.behaviorParam;

/**
 * Created by Adam on 6/29/2016.
 */
public class PrimableDriver {

    public static void main(String[] args) {


        Primable<Long> primableLong = n -> {

            for (int i = 2; i < n; i++) {
                if (n % i == 0)
                    return false;
            }
           // System.out.println(this.getClass().getName());
            return true;
        };

        System.out.println(primableLong.isPrime(101L));


        Primable<Integer> primable = new Primable<Integer>() {

            @Override
            public boolean isPrime(Integer n) {
                ////http://www.mkyong.com/java/how-to-determine-a-prime-number-in-java/
                for (int i = 2; i < n; i++) {
                    if (n % i == 0)
                        return false;
                }
                return true;
            }
        };

        System.out.println(primable.isPrime(101));


        Primable<String> primable2 = new Primable<String>() {
            @Override
            public boolean isPrime(String n) {
                return primable.isPrime(Integer.parseInt(n));
            }
        };

        System.out.println(primable2.isPrime("4"));




    }
}


