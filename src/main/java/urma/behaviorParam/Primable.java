package urma.behaviorParam;

/**
 * Created by Adam on 6/29/2016.
 */

@FunctionalInterface
public interface Primable<T> {

      boolean isPrime(T n);


}
