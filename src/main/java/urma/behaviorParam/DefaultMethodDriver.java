package urma.behaviorParam;

/**
 * Created by Adam on 6/29/2016.
 */
public class DefaultMethodDriver {

    //We need default methods because Collection does not have stream() method, among others.
    //and we'd like to use standard Collections for Java8 code. Check out the stream() method of Collections interface in SDK.


    @FunctionalInterface
    public interface Calculatable {
        int calc(int a ,int b);

        default int sum(int a, int b){
            System.out.println("summing");
            return a + b;
        }

        default int mult(int a, int b){
            System.out.println("multiplying");
            return a * b;
        }
    }

    public static void main(String[] args) {
        Calculatable calculatable = new Calculatable() {
            @Override
            public int calc(int a, int b) {
                if (a < 100)
                   return sum(a,b);
                else
                    return mult(a,b);
            }
        };


        System.out.println(calculatable.calc(4,5));
        System.out.println(calculatable.calc(100,5));

    }
}
