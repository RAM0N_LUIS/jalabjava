package urma.behaviorParam;

/**
 * Created by Adam on 7/15/2015.
 */

import javax.swing.Timer;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileFilter;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class _02BehaviorP1 {

    public static void main(String[] args) {

        //file filter example

        //FileFilter is a functional interface. Check the SDK (aka JDK)
        FileFilter fileFilter1 = new FileFilter() {
            @Override
            public boolean accept(File file) {
                return file.getName().endsWith(".pdf") ;
            }
        };

        FileFilter fileFilter2 = (File file) -> file.getName().endsWith(".pdf");

        FileFilter fileFilter3 = new FileFilter() {
            @Override
            public boolean accept(File pathname) {
                return pathname.getName().endsWith(".doc");
            }
        };


        //runnable example

        //Runnable is a functional interface
        Runnable runnable1 = () -> System.out.println( "helloworld from thread " + Thread.currentThread().getName());

        Runnable runnable2 = () -> System.out.println( "helloworld from thread " + Thread.currentThread().getName());




    }
}
