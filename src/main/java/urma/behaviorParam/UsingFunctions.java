package urma.behaviorParam;

import horstcore.ch10.sec07.InterruptionDemo;

import java.util.Date;
import java.util.function.BiFunction;
import java.util.function.Function;

/**
 * Created by Adam on 6/23/2016.
 */
public class UsingFunctions {

    public static void main(String[] args) {

        //crate a FUNCTION
        //takes and integer and returns a string

        //start out with an anonymous inner class then migrate to lambda
        String string = calc(integer -> {
            return String.valueOf( ":"+ integer);
            //return String.valueOf( ":"+ integer);
        },8);


        System.out.println(string);


        //crate a BI FUNCTION

        long stringFromBiFunction = calcBi(new BiFunction<Integer, Date, Long>() {
            @Override
            public Long apply(Integer integer, Date date) {
                return date.getTime() + integer;
            }
        }, 5, new Date());

        System.out.println(stringFromBiFunction);



        BiFunction<Integer,Date,Long> anotherBiFunction = (integer, date) -> date.getTime() + integer;

        System.out.println(anotherBiFunction.apply(67, new Date()));





    }

    //the first generic param is the explicty parameter to the fucntion and the second is the return value
    private static String calc(Function<Integer, String> function, Integer integer){
        return function.apply(integer);
    }

    private static Long calcBi(BiFunction<Integer, Date , Long> function, Integer integer, Date date){
        return function.apply(integer, date);
    }


}
