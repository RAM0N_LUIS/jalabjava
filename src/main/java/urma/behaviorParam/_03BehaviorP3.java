package urma.behaviorParam;

/**
 * Created by Adam on 7/15/2015.
 */

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class _03BehaviorP3 {

    public static void main(String[] args) {



        //RUNNER 1
        //some more examples of using type-anonymous objects to defer execution.
        Runnable runner1 = new Runnable() {
            @Override
            public void run() {
                //as soon as we use reflection, we can not convert this to a lambda
                System.out.println("hello from " + this.getClass().getName());
            }
        };


        ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);
        executor.scheduleAtFixedRate(runner1, 0, 1, TimeUnit.SECONDS);

        //RUNNER 2
        //Runnable is a functional interface in Java8 (check Runnable in the JDK)
        //What is a functional interface? An interface with exactly 1 non-default, non-overriding method.
        //So long as we don't use reflection or this, we can convert this to a lambda

        Runnable runner2 = new Runnable() {
            @Override
            public void run() {
                System.out.println("hello from runner2");
            }
        };


        new Thread(runner2).start();




    }
}
