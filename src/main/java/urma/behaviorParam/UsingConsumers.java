package urma.behaviorParam;

import horstcore.ch06.sec06.Arrays;

import java.util.Date;
import java.util.List;
import java.util.function.*;

/**
 * Created by Adam on 6/22/2016.
 */
public class UsingConsumers {

    public static void main(String[] args) {

        System.out.println("//Consumer");
        //Consumer
        Consumer<String> consumer = new Consumer<String>() {
            @Override
            public void accept(String s) {
                System.out.println(s.toLowerCase());
            }
        };

        //since a consumer returns void, it can not be passed into a printStream like System.out.println()
        //but you can create your own if you want.
        consumeMe(consumer, "ADAM");


        List<String> strings = Arrays.asList("HeLo", "GoOdByE");
        strings.stream()
                .forEach(consumer);


        System.out.println("//Bi Consumer");
        //BI Consumer
        BiConsumer<Integer, Date> biConsumer = (integer, date) -> {


            if (integer >= 0){
                for (int nC = 0; nC < integer; nC++) {
                    date.setTime(date.getTime() + (1 * 24 * 60 * 60 * 1000)); //add one day
                }
            } else {
                for (int nC = 0; nC > integer; nC--) {
                    date.setTime(date.getTime() - (1 * 24 * 60 * 60 * 1000)); //subtract one day
                }
            }

            System.out.println(date);
        };


        //add one week and print it out
        biConsumer.accept(7, new Date());




        //For performance, Java8 defines a whole set of Consumers that are optimized for numeric values.
        //These are Numeric Performance Functional Iterfaces.
        //Notice that they are NOT parameterized.

        System.out.println("//Int Consumer");
        //INT Consumer
        IntConsumer intConsumer = new IntConsumer() {
            @Override
            public void accept(int value) {
                System.out.println(value + ": " + (value % 2 == 0 ? "EVEN" : "ODD"));

            }
        };

        List<Integer> intList = Arrays.asList(54, 5, 8, 5, 5, 2, 85, 9954, 555, 4771, 6544);

        for (Integer integer : intList) {
            intConsumer.accept(integer);
        }


        System.out.println("//Double Consumer");
        //Double Consumer
        DoubleConsumer dubConsumer = new DoubleConsumer() {

            @Override
            public void accept(double value) {

                System.out.println(Math.pow(value, 2));
            }
        };

        List<Double> dubList = Arrays.asList(85.63, 9954.966, 555.32, 4771.1, 6544.587);

        for (Double aDouble : dubList) {
            dubConsumer.accept(aDouble);
        }


        System.out.println("//Long Consumer");
        //Long Consumer
        LongConsumer longConsumer = new LongConsumer() {

            @Override
            public void accept(long value) {

                System.out.println(value - 1);
            }
        };


        longConsumer.accept(55555L);


        System.out.println("//Call method with function as parameter");
        //Call method with function as parameter

        goBackInTime(biConsumer, 14, new Date());


    }


    private static void goBackInTime(BiConsumer<Integer, Date> biConsumer, Integer integer, Date date){
        biConsumer.accept(-integer, date);
    }

    private static void consumeMe(Consumer<String> consumer, String string){
        consumer.accept(string);
    }


}
