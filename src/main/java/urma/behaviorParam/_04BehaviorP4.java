package urma.behaviorParam;

/**
 * Created by Adam on 7/15/2015.
 */

import java.util.Comparator;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class _04BehaviorP4 {

    public static void main(String[] args) {



        //this ia a functional interface - check the SDK. So longa as no "this" or relfection, I can convert this
        //to ao lambda.
        //look at the SDK. Notice that compare() is non-overrding and non-default. equals() overrides from Object.
        Comparator<String> comparator2 = new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return Integer.compare(o1.length(), o2.length());
            }
        };


        Comparator<String> comparator1 = (o1, o2) -> Integer.compare(o1.length(), o2.length());



    }
}
