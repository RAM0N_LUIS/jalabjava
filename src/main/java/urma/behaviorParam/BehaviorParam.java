package urma.behaviorParam;

/**
 * Created by Adam on 7/14/2015.
 */
import javax.swing.Timer;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;
import java.util.function.Function;

public class BehaviorParam {


    public static void main(String[] args) {


        //the evolution of behavior parameterization
        //anonymous inner classes
        new Thread(() -> {
            try {
                Toolkit.getDefaultToolkit().beep();
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();



        //using a class that we define.
        //we can also wrap this into a class if we wanted
        class MyActionListener implements  ActionListener {

            @Override
            public void actionPerformed(ActionEvent e) {
                    Toolkit.getDefaultToolkit().beep();

            }
        }

        new Timer(750, new MyActionListener()).start();


        //using an anon inner class
        new Timer(750, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Toolkit.getDefaultToolkit().beep();
            }
        }).start();


        //using a lambda
        //similar using swing actionListener using swing adn the java event model with no gui
        new Timer(250, evt -> {
            Toolkit.getDefaultToolkit().beep();
            System.out.println(evt.getSource());
        }).start();






    }







}
