package urma._var_capture;

import horstcore.ch06.sec06.Arrays;
import horstcore.ch10.sec07.InterruptionDemo;

import java.util.List;

/**
 * Created by Adam on 6/29/2016.
 */
public class LocalExample {

    public static void main(String[] args) {
        List<Integer> integerList = Arrays.asList(1,2,3);
        int var = 10;
        integerList.forEach(x -> System.out.println(x + var));  //this is capturing the local variable and it's "effectively" final.

    }
}
