package urma._var_capture;

import horstcore.ch06.sec06.Arrays;

import java.util.List;

/**
 * Created by Adam on 6/29/2016.
 */
public class LocalExampleNotWorking {

    public static void main(String[] args) {
        List<Integer> integerList = Arrays.asList(1,2,3);
        int var = 10;
       // integerList.forEach(x -> System.out.println(x + var++));  //try to increment the var and the compiler will complain
        integerList.forEach(x -> System.out.println(x + var));  //try to increment the var and the compiler will complain

    }
}
