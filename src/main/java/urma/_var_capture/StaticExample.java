package urma._var_capture;

import horstcore.ch06.sec06.Arrays;

import java.util.List;

/**
 * Created by Adam on 6/29/2016.
 */
public class StaticExample {
    private static int var = 10; //this is effecively final
    public static void main(String[] args) {
        List<Integer> integerList = Arrays.asList(1, 2, 3);

        integerList.forEach(x -> System.out.println(x + var));  //this is capturing the static variable and it's "effectively" final.

        System.out.println(var);

    }
}
