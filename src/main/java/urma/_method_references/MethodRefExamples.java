package urma._method_references;

import horstcore.ch06.sec06.Arrays;
import urma.behaviorParam.Primable;

import java.util.Date;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;

/**
 * Created by Adam on 6/29/2016.
 */
public class MethodRefExamples {


    public static void main(String[] args) {
        List<String> letters = Arrays.asList("a", "b", "c", "d", "e");

        System.out.println("lambda example");
        //using lambda
        long start = System.currentTimeMillis();
        letters.stream().forEach(s -> System.out.println(s));
        long end = System.currentTimeMillis();
        System.out.println(end - start);

        System.out.println("faster example");
        //using method references (much faster)
        long start2 = System.currentTimeMillis();
        letters.stream().forEach(System.out::println);
        long end2 = System.currentTimeMillis();
        System.out.println(end2 - start2);

        System.out.println("contains example");

        String strName = "Adam";
        long start3 = System.currentTimeMillis();
        letters.stream()
                .filter(strName::contains)
                //replace with lambda or method reference either one. Be comforatable switching among all three.
                .map(s -> s.toUpperCase())
                .forEach(System.out::println);
        long end3 = System.currentTimeMillis();
        System.out.println(end3 - start3);

    }




//    public static void main(String[] args) {
//
//        printPrime(new Primable() {
//            @Override
//            public boolean isPrime(int n) {
//                return false;
//            }
//        }, 101);
//    }
//
//
//    private static void printPrime(Primable primable, int n){
//        System.out.println(n + " is prime " + primable.isPrime(n));
//    }
}
