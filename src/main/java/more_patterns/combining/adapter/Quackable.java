package more_patterns.combining.adapter;

public interface Quackable {
	public void quack();
}
