package more_patterns.combining.observer;

public interface Quackable extends QuackObservable {
	public void quack();
}
