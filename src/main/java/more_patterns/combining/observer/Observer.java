package more_patterns.combining.observer;

public interface Observer {
	public void update(QuackObservable duck);
}
