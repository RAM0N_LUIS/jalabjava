package more_patterns.combining.ducks;

public interface Quackable {
	public void quack();
}
