package more_patterns.combining.factory;

public interface Quackable {
	public void quack();
}
