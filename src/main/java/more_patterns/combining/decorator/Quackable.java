package more_patterns.combining.decorator;

public interface Quackable {
	public void quack();
}
