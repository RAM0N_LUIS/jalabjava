package more_patterns.factory.pizzaaf;

public interface Pepperoni {
	public String toString();
}
