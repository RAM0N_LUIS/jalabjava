package more_patterns.factory.pizzaaf;

public interface Dough {
	public String toString();
}
