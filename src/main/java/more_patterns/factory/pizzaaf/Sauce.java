package more_patterns.factory.pizzaaf;

public interface Sauce {
	public String toString();
}
