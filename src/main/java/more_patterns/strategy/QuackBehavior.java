package more_patterns.strategy;

public interface QuackBehavior {
	public void quack();
}
