package more_patterns.strategy;

public interface FlyBehavior {
	public void fly();
}
