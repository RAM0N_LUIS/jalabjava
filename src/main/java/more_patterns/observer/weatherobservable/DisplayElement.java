package more_patterns.observer.weatherobservable;

public interface DisplayElement {
	public void display();
}
