package more_patterns.observer.weather;

public interface DisplayElement {
	public void display();
}
