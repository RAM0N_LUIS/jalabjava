package more_patterns.ducks;

public interface Duck {
	public void quack();
	public void fly();
}
