package more_patterns.collections.iterator;

public interface Menu {
	public Iterator createIterator();
}
