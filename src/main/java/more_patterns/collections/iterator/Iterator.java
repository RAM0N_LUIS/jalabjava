package more_patterns.collections.iterator;

public interface Iterator {
	boolean hasNext();
	Object next();
}
