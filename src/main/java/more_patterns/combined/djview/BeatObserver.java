package more_patterns.combined.djview;
  
public interface BeatObserver {
	void updateBeat();
}
