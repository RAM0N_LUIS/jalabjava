package more_patterns.combined.djview;
  
public interface BPMObserver {
	void updateBPM();
}
