package more_patterns.adapter.ducks;

public interface Duck {
	public void quack();
	public void fly();
}
