package more_patterns.command.simpleremoteWL;

public interface Command {
	public void execute();
}
