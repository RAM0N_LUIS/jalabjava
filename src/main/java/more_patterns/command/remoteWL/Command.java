package more_patterns.command.remoteWL;

public interface Command {
	public void execute();
}
