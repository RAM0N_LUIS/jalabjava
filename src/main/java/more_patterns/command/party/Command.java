package more_patterns.command.party;

public interface Command {
	public void execute();
	public void undo();
}
