package more_patterns.command.party;

public class NoCommand implements Command {
	public void execute() { }
	public void undo() { }
}
