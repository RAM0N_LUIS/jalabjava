package more_patterns.command.remote;

public class NoCommand implements Command {
	public void execute() { }
}
