package more_patterns.command.remote;

public interface Command {
	public void execute();
}
