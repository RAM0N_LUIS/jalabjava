package more_patterns.command.simpleremote;

public interface Command {
	public void execute();
}
