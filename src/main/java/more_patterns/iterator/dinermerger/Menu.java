package more_patterns.iterator.dinermerger;

public interface Menu {
	public Iterator createIterator();
}
