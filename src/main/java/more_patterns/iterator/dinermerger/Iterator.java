package more_patterns.iterator.dinermerger;

public interface Iterator {
	boolean hasNext();
	MenuItem next();
}
