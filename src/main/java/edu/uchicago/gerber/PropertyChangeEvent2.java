package edu.uchicago.gerber;

import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.property.SimpleIntegerProperty;

public class PropertyChangeEvent2 {
    public static void main(String[] args) {
        SimpleIntegerProperty xProperty = new SimpleIntegerProperty(0);

        // Adding a invalidation listener (anonymous inner class)
        xProperty.addListener(new InvalidationListener() {
            @Override
            public void invalidated(Observable o) {
                System.out.println(o.toString());
            }
        });

        xProperty.set(50);
        xProperty.set(51);
        xProperty.set(52);


    }
}

//http://www.java2s.com/Tutorials/Java/JavaFX/0200__JavaFX_Properties.htm
//the difference between a ChangeListener and an invalidationListener.
//Using a ChangeListener we will get the Observable (ObservableValue), the old value, and the new value
//  Using the invalidationListener only gets the Observable object (property)