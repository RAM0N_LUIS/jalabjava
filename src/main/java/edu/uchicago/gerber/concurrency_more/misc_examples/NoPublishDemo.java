package edu.uchicago.gerber.concurrency_more.misc_examples;



/**
 * <a href="http://hllvm.group.iteye.com/group/topic/34932">请问R大 有没有什么工具�?�以查看正在�?行的类的c/汇编代�?</a>�??到了<b>代�?�??�?�</b>的问题。
 *
 * @author Jerry Lee (oldratlee at gmail dot com)
 * @see <a href="http://hllvm.group.iteye.com/group/topic/34932">请问R大 有没有什么工具�?�以查看正在�?行的类的c/汇编代�?</a>
 */
public class NoPublishDemo {
    boolean stop = false;

    public static void main(String[] args) {
        // LoadMaker.makeLoad();

        NoPublishDemo demo = new NoPublishDemo();

        Thread thread = new Thread(demo.getConcurrencyCheckTask());
        thread.start();

        Utils.sleep(1000);
        System.out.println("Set stop to true in main!");
        demo.stop = true;
        System.out.println("Exit main.");
    }

    ConcurrencyCheckTask getConcurrencyCheckTask() {
        return new ConcurrencyCheckTask();
    }

    private class ConcurrencyCheckTask implements Runnable {
        @Override
        public void run() {
            System.out.println("ConcurrencyCheckTask started!");
            // 如果主线中stop的值�?��?，则循环会退出。
            // 在我的开�?�机上，几乎必现循环�?退出�?（简�?�安全的解法：在running属性上加上volatile）
            while (!stop) {
            }
            System.out.println("ConcurrencyCheckTask stopped!");
        }
    }
}
