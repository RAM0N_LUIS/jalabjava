/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.uchicago.gerber.concurrency_more.basic.sharingobject;

/**
 * ThreadSafe mutable Integer holder
 * @author vijay
 */

public class SynchronizedInteger {
   private int value;

    public synchronized int getValue() {
        return value;
    }

    public synchronized void setValue(int value) {
        this.value = value;
    }
   
}
