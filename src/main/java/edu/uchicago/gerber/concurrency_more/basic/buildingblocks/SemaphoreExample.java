/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.uchicago.gerber.concurrency_more.basic.buildingblocks;

import java.util.concurrent.Semaphore;

/**
 *
 * @author vijay
 */
public class SemaphoreExample {
    //this semaphore has 10 permissions
    Semaphore semaphore = new Semaphore(10);
    int i;

    synchronized void callCommonResource() throws InterruptedException{
        //acquire removes a permission
        semaphore.acquire();
        i++;
        System.out.println(i);
        //release adds a permission
        semaphore.release();
    }

    public static void main(String[] args) {
        final SemaphoreExample se = new SemaphoreExample();
        Runnable r = new Runnable() {

            public void run() {
                while (true) {
                    try {                        
                        se.callCommonResource();
                    } catch (InterruptedException ie) {
                    }
                    
                }
            }
        };
        Thread t1 = new Thread(r);
        Thread t2 = new Thread(r);
        
        t1.start();
        t2.start();
        
    }
}
