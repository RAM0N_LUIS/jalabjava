package edu.uchicago.gerber.concurrency_more.basic.concurrency;

public class Data {
	private String value;
	
	public Data(String value) {
		this.value = value;
	}
	
	public String getValue() {
		return this.value;
	}
	
	public void setValue(String value) {
		this.value = value;
	}
}
