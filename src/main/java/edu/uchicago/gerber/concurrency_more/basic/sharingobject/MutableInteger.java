/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.uchicago.gerber.concurrency_more.basic.sharingobject;

/**
 * NotThreadSafe mutable Integer holder
 * @author vijay
 */

public class MutableInteger {
   private int value;

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
   
}
