package edu.uchicago.gerber.web_service_capture;

import com.google.gson.Gson;
import edu.uchicago.gerber.web_service_capture.gsonjigs.CountryData;
import edu.uchicago.gerber.web_service_capture.gsonjigs.RestResponse;
import edu.uchicago.gerber.web_service_capture.gsonjigs.Result;

import java.util.ArrayList;
import java.util.concurrent.*;

/**
 * Created by Adam on 6/29/2016.
 */

//you can use this website to search for api's
//http://www.programmableweb.com/apis/directory


//go to this website to generate gson jigs
//http://www.jsonschema2pojo.org/
public class CaptureWebDriver {

    public static void main(String[] args) {
        ExecutorService exService = Executors.newSingleThreadExecutor();
        FutureTask<ArrayList<Result>> futureTask= new FutureTask<ArrayList<Result>>(new GetResultsCallable());
        exService.execute(futureTask);
//        //checks if task done
//        System.out.println(futureTask.isDone());
//        //checks if task canceled
//        System.out.println(futureTask.isCancelled());
        //fetches result and waits if not ready
        try {
            ArrayList<Result> results = futureTask.get();
            for (Result result : results) {
                System.out.println(result.getName() + " : " + result.getAlpha3Code());
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }


    }
}

class GetResultsCallable implements Callable<ArrayList<Result>> {
    public static final String HTTP_SERVICES_GROUPKT_COM_COUNTRY_GET_ALL = "http://services.groupkt.com/country/get/all";
    public ArrayList<Result> call() {

        String str = null;
        try {
            str = JsonParser.sendGet(HTTP_SERVICES_GROUPKT_COM_COUNTRY_GET_ALL);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (str == null){
            System.out.println("Sorry, I couldn't find your data");
        }

        CountryData countryRestData = new Gson().fromJson(str, CountryData.class);
        RestResponse restResponse = countryRestData.getRestResponse();
        ArrayList<Result> resultsList = new ArrayList<>(restResponse.getResult());
        ArrayList<String> messageList = new ArrayList<>(restResponse.getMessages());


        return resultsList;
    }
}
