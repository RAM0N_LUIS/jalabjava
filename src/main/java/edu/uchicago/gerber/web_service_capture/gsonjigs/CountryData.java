
package edu.uchicago.gerber.web_service_capture.gsonjigs;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class CountryData {

    @SerializedName("RestResponse")
    @Expose
    private RestResponse restResponse;

    /**
     * 
     * @return
     *     The restResponse
     */
    public RestResponse getRestResponse() {
        return restResponse;
    }

    /**
     * 
     * @param restResponse
     *     The RestResponse
     */
    public void setRestResponse(RestResponse restResponse) {
        this.restResponse = restResponse;
    }

}
