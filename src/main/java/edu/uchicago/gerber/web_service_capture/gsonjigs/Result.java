
package edu.uchicago.gerber.web_service_capture.gsonjigs;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class Result {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("alpha2_code")
    @Expose
    private String alpha2Code;
    @SerializedName("alpha3_code")
    @Expose
    private String alpha3Code;

    /**
     * 
     * @return
     *     The name
     */
    public String getName() {
        return name;
    }

    /**
     * 
     * @param name
     *     The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 
     * @return
     *     The alpha2Code
     */
    public String getAlpha2Code() {
        return alpha2Code;
    }

    /**
     * 
     * @param alpha2Code
     *     The alpha2_code
     */
    public void setAlpha2Code(String alpha2Code) {
        this.alpha2Code = alpha2Code;
    }

    /**
     * 
     * @return
     *     The alpha3Code
     */
    public String getAlpha3Code() {
        return alpha3Code;
    }

    /**
     * 
     * @param alpha3Code
     *     The alpha3_code
     */
    public void setAlpha3Code(String alpha3Code) {
        this.alpha3Code = alpha3Code;
    }

}
