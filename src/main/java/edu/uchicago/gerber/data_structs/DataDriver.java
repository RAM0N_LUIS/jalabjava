package edu.uchicago.gerber.data_structs;

import horstcore.ch07.sec04.LinkedHashMapDemo;

import java.util.*;

/**
 * Created by Adam on 7/7/2016.
 */
public class DataDriver {

    public static void main(String[] args) {
        //O(log(n) but sorted alpha by key

        TreeMap<Integer, String> treeMap = new TreeMap<>();
        TreeSet<String> treeSet = new TreeSet<>();


        //O(1) but not sorted.
        HashMap<Integer,String> hashMap = new HashMap<>(301);
        HashSet<String> hashSet = new HashSet<>();

        //O(1) and sorted
        LinkedHashMap<Integer,String> linkedHashMap = new LinkedHashMap<>(301);

        for (int nC = 0; nC < 220; nC++) {
            treeMap.put(nC, "TreeMapValue"+nC);
            hashMap.put(nC, "HashMapValue"+nC);


            treeSet.add("TreeSet"+nC);
            hashSet.add("TreeSet" + nC);


            linkedHashMap.put(nC, "HashMapValue"+nC);
        }

        //http://stackoverflow.com/questions/559839/big-o-summary-for-java-collections-framework-implementations





    }
}
