package edu.uchicago.gerber;

import javafx.beans.binding.NumberBinding;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
/*from  w  ww  . j a v  a 2  s.  c o m*/
public class BindingHighLevel {
    public static void main(String[] args) {
        // Area = width * height
        IntegerProperty width = new SimpleIntegerProperty(10);
        IntegerProperty height = new SimpleIntegerProperty(10);
        NumberBinding area = width.multiply(height);
        System.out.println(area.getValue());
    }
}