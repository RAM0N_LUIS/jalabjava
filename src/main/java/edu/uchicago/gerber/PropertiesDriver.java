package edu.uchicago.gerber;
import javafx.beans.property.ReadOnlyStringProperty;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
/**
 * Created by Adam on 7/6/2016.
 */
public class PropertiesDriver {
    public static void main(String[] args) {

        //read only property
        ReadOnlyStringWrapper userName = new ReadOnlyStringWrapper("UChicago");


        //read-write preoperties
        StringProperty password  = new SimpleStringProperty("my-password");
        password.set("another-password"); //call set() or setValue()

        //notice the difference between the two calls here
        System.out.println(userName.getReadOnlyProperty() + " "  + password.get() ); //call get() or getValue()
        System.out.println(userName.getReadOnlyProperty().getValue() + " "  + password.get() ); //call get() or getValue()
    }
}
