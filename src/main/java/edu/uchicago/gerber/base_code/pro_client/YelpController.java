package edu.uchicago.gerber.base_code.pro_client;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import edu.uchicago.gerber.yelp.Yelp;
import edu.uchicago.gerber.yelp.YelpSearchResults;

import java.awt.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.animation.Animation;
import javafx.animation.Interpolator;
import javafx.animation.RotateTransition;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.concurrent.Worker;
import static javafx.concurrent.Worker.State.CANCELLED;
import static javafx.concurrent.Worker.State.FAILED;
import static javafx.concurrent.Worker.State.READY;
import static javafx.concurrent.Worker.State.RUNNING;
import static javafx.concurrent.Worker.State.SCHEDULED;
import static javafx.concurrent.Worker.State.SUCCEEDED;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.effect.BoxBlur;
import javafx.scene.effect.DropShadow;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.util.Duration;

import com.lynden.gmapsfx.GoogleMapView;
import com.lynden.gmapsfx.MapComponentInitializedListener;
import com.lynden.gmapsfx.javascript.object.GoogleMap;
import com.lynden.gmapsfx.javascript.object.LatLong;
import com.lynden.gmapsfx.javascript.object.MapOptions;

import com.lynden.gmapsfx.javascript.object.Marker;
import com.lynden.gmapsfx.javascript.object.MarkerOptions;

/**
 * FXML Controller class
 *
 * @author ag
 */
public class YelpController implements Initializable {
    @FXML
    private TextField txtSearch;
    @FXML
    private TextField txtCity;
    @FXML
    private Button btnGo;
    @FXML
    private ListView<String> lstView;
    @FXML
    private Label lblStatus;
    @FXML
    private ProgressIndicator prg;
    @FXML
    private Label lblTitle;



    YelpSearchResults yelpSearchResultLocal;
    /**  YelpSearchResults yelpSearchResultLocal;
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    // create a service.
    final Service yelpFinder = new Service<ObservableList<String>>() {
      @Override protected Task createTask() {
        return new Task<ObservableList<String>>() {
          @Override protected ObservableList<String> call() throws InterruptedException {
            updateMessage("Finding yelp data . . .");
//            updateProgress(0, 10);
//            for (int i = 0; i < 10; i++) {
//              Thread.sleep(300);
//              updateProgress(i + 1, 10);
//            }
            
            Yelp yelpApi = new Yelp();
            yelpSearchResultLocal = yelpApi.searchMultiple(txtSearch.getText(), txtCity.getText());
//            for (String str : yelpSearchResultLocal.getSimpleValues()) {
//                mListModel.addElement(str);
//            }
            
            updateMessage("Found yelp data.");
            return FXCollections.observableArrayList(yelpSearchResultLocal.getSimpleValues());
          }
        };
      }
    };
 
    // bind interesting service properties to the controls.
    lblStatus.textProperty().bind(yelpFinder.messageProperty());
    btnGo.disableProperty().bind(yelpFinder.runningProperty());
    lstView.itemsProperty().bind(yelpFinder.valueProperty());
   // prg.progressProperty().bind(yelpFinder.progressProperty());
  //  prg.visibleProperty().bind(yelpFinder.progressProperty().isNotEqualTo(new SimpleDoubleProperty(ProgressBar.INDETERMINATE_PROGRESS)));
 
    // kick off the service on an action.
    btnGo.setOnAction(new EventHandler<ActionEvent>() {
      @Override public void handle(ActionEvent actionEvent) {
        if (!yelpFinder.isRunning()) {
          yelpFinder.reset();
          yelpFinder.start();
        }
      }
    });

        lstView.setOnMouseClicked(me -> {
            try {

                if (!(me.getTarget() instanceof Text)){
                    return;
                }

                String strClicked = ((Text)me.getTarget() ).getText().toString();
                strClicked = strClicked.substring(0, strClicked.lastIndexOf('|')-1);
                String strName = strClicked.substring(0, strClicked.indexOf('|')-1).trim();
                String strAddress = strClicked.substring(strClicked.indexOf('|')+1, strClicked.length() ).trim();
                strName = strName.replaceAll(" ", "+");
                strAddress = strAddress.replaceAll(" ", "+");


                openWebpage(new URI("http://maps.google.com/maps/api/staticmap?center=" +
                        strName + "," + strAddress + "," +
                        txtCity.getText().replaceAll(" ","").trim() +
                        "&zoom=14&size=512x512&maptype=roadmap"));

            } catch (Exception e) {
               //do nothing for now
               e.printStackTrace();
            }


        } );
 
    // create a distracting square with different colors for different states.
    final Color normalDistractorColor = Color.BURLYWOOD;
    final Color runningDistractorColor = Color.DARKGREEN;
    final Color highlightedDistractorColor = Color.FIREBRICK;
    final Rectangle distractor = new Rectangle(25, 25, normalDistractorColor);
    distractor.setUserData(false); // maintains whether or not the mouse is in the distractor.
    distractor.setOpacity(0.8);
    Tooltip.install(distractor, new Tooltip("If you are looking for friends, you can click on me to stop searching"));
 
    // rotate the distractor to show that animation and stuff still continues while the work is being done.
    RotateTransition rt = new RotateTransition(Duration.millis(3000), distractor);
    rt.setByAngle(360);
    rt.setCycleCount(Animation.INDEFINITE);
    rt.setInterpolator(Interpolator.LINEAR);
    rt.play();
 
    // create some effects for the animation.
    final BoxBlur normal      = new BoxBlur();
    final BoxBlur highlighted = new BoxBlur(); highlighted.setInput(new DropShadow());
    distractor.setEffect(normal);
 
    // the distracting animation can be used to cancel the friend lookup.
    distractor.setOnMouseClicked(new EventHandler<MouseEvent>() {
      @Override public void handle(MouseEvent mouseEvent) {
        if (yelpFinder.isRunning()) {
          yelpFinder.cancel();
        }
      }
    });
    distractor.setOnMouseEntered(new EventHandler<MouseEvent>() {
      @Override public void handle(MouseEvent mouseEvent) {
        distractor.setUserData(true);
        if (yelpFinder.isRunning()) {
          distractor.setEffect(highlighted);
          distractor.setFill(highlightedDistractorColor);
        }
      }
    });

    distractor.setOnMouseExited(new EventHandler<MouseEvent>() {
      @Override public void handle(MouseEvent mouseEvent) {
        distractor.setUserData(false);
        distractor.setEffect(normal);
        distractor.setFill(normalDistractorColor);
      }
    });
 
    // do something when the service has succeeded.
    yelpFinder.stateProperty().addListener(new ChangeListener<Worker.State>() {
      @Override public void changed(ObservableValue<? extends Worker.State> observableValue, Worker.State oldState, Worker.State newState) {
        switch (newState) {
          case SCHEDULED:
            lblStatus.setVisible(false);
            prg.progressProperty().bind(yelpFinder.progressProperty());  // workaround, we should be able to permanently bind to the progress, but unless we do this sometimes the progress does not always reach the end.
            break;
          case READY:
          case RUNNING:
            lblStatus.setVisible(false);
            break;
          case SUCCEEDED:
            lblStatus.setVisible(true);
           // lblStatus.setText("Let's grab a beer.");
            prg.progressProperty().unbind();
            prg.setProgress(1);  // workaround, we should be able to permanently bind to the progress, but unless we do this sometimes the progress does not always reach the end. (even this workaround didn't work, so I have no idea about this...)
            break;
          case CANCELLED:
          case FAILED:
            lblStatus.setVisible(true);
            lblStatus.setText("Bummer dude, party's over.");
            break;
        }
      }
    });
 
    // maintain the distractor colors while the service is running.
    yelpFinder.runningProperty().addListener(new ChangeListener<Boolean>() {
      @Override public void changed(ObservableValue<? extends Boolean> observableValue, Boolean aBoolean, Boolean isRunning) {
        if (isRunning) {
          if (!(Boolean) distractor.getUserData()) {
            distractor.setEffect(normal);
            distractor.setFill(runningDistractorColor);
          } else {
            distractor.setEffect(highlighted);
            distractor.setFill(highlightedDistractorColor);
          }
        } else {
          distractor.setEffect(normal);
          distractor.setFill(normalDistractorColor);
        }
      }
    });
    }

    public static void openWebpage(URI uri) {
        Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
        if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
            try {
                desktop.browse(uri);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    
 
}
