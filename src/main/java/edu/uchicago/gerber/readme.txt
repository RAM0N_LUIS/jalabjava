BasicGroupMain  - a driver class that shows how to use a Group as your root node. 
BasicRegionMain  - a driver class that shows how to use a Region as your root node and FXML with SceneBuilder.
BindMain - driver that shows how to create thread-safe properties in your own custom classes and listen for changes in 
those properties.
CssExampleMain - driver used in conjunction with CssExample.css, CssExampleController.java, and cssexample.css to show how 
to use CSS to style your apps. 
RsvpMain - base code for Rsvp reader. 

//DragDropMain - driver showing how to read files from drag-drop operation from OS

LaboriousLayoutMain - driver showing an anti-pattern for laying-out an Region with hard-code. You should always use 
SceneBuilder for Regions.

MyAppMain - a very simple fxml project with a clickable button.


PbarsMain - driver showing how to use progress bars.

PdfMain- base code for next project. 


YelpMain - driver showing the use of a web service. 

TimesMain - driver showing how to capture urls from a web-service. This will be useful for your next project. 

TabBrowserMain - driver showing the use tabs and browser. This will be useful for the RSVP app. 


ThreadsMain - driver showing the difference between the two concrete implementations of Worker: Task and Service. Good example.

SwingFxMain - driver showing integrating (embedding) Swing into javaFX. 

ProducerCosnumerMain - This implements the producer-consumer design pattern to relay work from one executor to another.


&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
to run drivers directly, right-click the jaLabFxAdvanced project || Custom || Goals...

clean package exec:java -Dexec.mainClass="edu.uchicago.gerber.TheDriverMain"

