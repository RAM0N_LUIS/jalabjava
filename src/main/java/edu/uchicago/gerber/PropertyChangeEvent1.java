package edu.uchicago.gerber;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
//http://www.java2s.com/Tutorials/Java/JavaFX/0200__JavaFX_Properties.htm
public class PropertyChangeEvent1 {
    public static void main(String[] args) {
        SimpleIntegerProperty xProperty = new SimpleIntegerProperty(0);

        // Adding a change listener with anonymous inner class
        xProperty.addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> ov, Number oldVal,
                                Number newVal) {
                System.out.println("old value:"+oldVal);
                System.out.println("new value:"+newVal);
            }
        });


        xProperty.set(5);


    }
}