package edu.uchicago.gerber.generic;

/**
 * Created by Adam on 7/6/2016.
 */
public class SampleGeneric<T> {

    private T data;

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
