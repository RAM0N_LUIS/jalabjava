package edu.uchicago.gerber.generic;

import java.io.Serializable;
import java.util.Comparator;

/**
 * Created by Adam on 7/6/2016.
 */

//this is an "upper bounds"
public class ComparableClass<T extends  Comparable & Serializable >  {

    private T data;


    public ComparableClass(T data) {
        this.data = data;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public int howDoIcompare(ComparableClass<String> t){
      return  (this.getData()).compareTo(t.getData());
    }



}
