package edu.uchicago.gerber.generic;

/**
 * Created by Adam on 7/6/2016.
 */
public class TwoTypePair<T, U> {

    private T first;
    private U second;

    public TwoTypePair(T first, U second) {
        this.first = first;
        this.second = second;
    }

    public T getFirst() {
        return first;
    }

    public void setFirst(T first) {
        this.first = first;
    }

    public U getSecond() {
        return second;
    }

    public void setSecond(U second) {
        this.second = second;
    }

    @Override
    public String toString() {
        return "TwoTypePair{" +
                "first=" + first +
                ", second=" + second +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TwoTypePair<?, ?> that = (TwoTypePair<?, ?>) o;

        if (!first.equals(that.first)) return false;

        return second.equals(that.second);

    }




}
