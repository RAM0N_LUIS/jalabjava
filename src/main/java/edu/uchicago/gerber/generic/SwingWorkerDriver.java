package edu.uchicago.gerber.generic;

import javax.swing.*;

/**
 * Created by Adam on 7/7/2016.
 */
public class SwingWorkerDriver {


    public static void main(String[] args) {

        SwingWorker<Integer,Integer> swingWorker = new SwingWorker<Integer, Integer>() {
            @Override
            protected Integer doInBackground() throws Exception {
                Thread.sleep(1000);
                return 42;
            }

            protected void done()
            {
                try
                {
                    System.out.println("done");
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        };



    }


}
