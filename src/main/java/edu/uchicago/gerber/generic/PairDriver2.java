package edu.uchicago.gerber.generic;

/**
 * Created by Adam on 7/6/2016.
 */
public class PairDriver2 {


    public static void main(String[] args) {
        Pair<String> parString = new Pair<>();
        Pair<String> parString2 = new Pair<>("Adam", "Gerber");
        parString.setFirst("Adam");
        parString.setSecond("Gerber");

        System.out.println(parString.equals(parString2));
    }
}
