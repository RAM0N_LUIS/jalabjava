package edu.uchicago.gerber.generic;

/**
 * Created by Adam on 7/6/2016.
 */
public class ComarableDriver {

    public static void main(String[] args) {
        ComparableClass<String> stringComparableAdam = new ComparableClass("Adam");
        ComparableClass<String> stringComparableRobert = new ComparableClass("Robert");

        int nCompare = stringComparableAdam.howDoIcompare(stringComparableRobert);
        System.out.println(nCompare);
    }
}
