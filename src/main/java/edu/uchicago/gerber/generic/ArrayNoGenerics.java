package edu.uchicago.gerber.generic;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Adam on 7/5/2016.
 */
public class ArrayNoGenerics {

    public static void main(String[] args) {

        List stringList1 = new ArrayList();
        stringList1.add("Java");
        stringList1.add(new Integer(545));


        //be careful of the cast!!!
        String s1 = (String) stringList1.get(0);

    }
}
