package edu.uchicago.gerber.generic;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Adam on 7/6/2016.
 */
public class MoreGenericsDriver {

    public static void main(String[] args) {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                System.out.println("hello there from my runnable");
            }
        };

        MoreGenerics.runSomething(runnable);
        List<Object> list= new ArrayList<>();
        list.add(new Double(6545));
        list.add(new Double(86789));


        List<Double> list2= new ArrayList<>();
        list.add(new Double(6545));
        list.add(new Double(86789));

        MoreGenerics.doSomething(list);
        MoreGenerics.doSomethingAgain(list2);

        MoreGenerics.printSomething(45.36);

        List<Float> listFloats = new ArrayList<>();
        listFloats.add(12.3f);
        listFloats.add(124.3f);
        listFloats.add(1672.3f);

         List<Double> dublist = MoreGenerics.retunrDoubleList(listFloats);
        for (Double aDouble : dublist) {
            System.out.println(aDouble);
        }

        Date date =   MoreGenerics.returnSomething(new Date());
        System.out.println(date);


        MoreGenerics.doSomethingAgainSuper(list2);

    }
}
