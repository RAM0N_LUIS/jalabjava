package edu.uchicago.gerber.generic;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Adam on 7/7/2016.
 */
public class ArrayListExample {

    public static void main(String[] args) {
        //let's create an ArrayList which is paremeterized
        List<Integer> list = new ArrayList<>();

        //this will use autoboxign to box up the int's to Integers
        list.add(654);
        list.add(7878);
        list.add(36545);
        list.add(78956);

        for (Integer integer : list) {
            System.out.println(integer);
        }




    }
}
