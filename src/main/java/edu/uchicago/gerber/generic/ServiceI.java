package edu.uchicago.gerber.generic;

/**
 * Created by Adam on 7/7/2016.
 */



public interface ServiceI<R, T> {
    R executeService(T...args);
}
