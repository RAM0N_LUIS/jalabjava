package edu.uchicago.gerber.generic;

/**
 * Created by Adam on 7/7/2016.
 */
public class Sample<T> {

    private T data;

    public Sample(T t) {
        data = t;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
