package edu.uchicago.gerber.generic;

/**
 * Created by Adam on 7/6/2016.
 */
public class Pair<T> {

    private T first;
    private T second;

    public Pair(T first, T second) {
        this.first = first;
        this.second = second;
    }

    public Pair() {
        first = null;
        second = null;
    }

    public T getFirst() {
        return first;
    }

    public void setFirst(T first) {
        this.first = first;
    }

    public T getSecond() {
        return second;
    }

    public void setSecond(T second) {
        this.second = second;
    }

    @Override
    public String toString() {
        return "Pair{" +
                "first=" + first +
                ", second=" + second +
                '}';
    }

    @Override
    public boolean equals(Object o) {


        if (o == null){
            return false;
        } else if (getClass() != o.getClass()){
            return false;
        } else {
            Pair<T> otherPair = (Pair<T>) o;
            return (first.equals(otherPair.first) && second.equals(otherPair.second));

        }

    }

    @Override
    public int hashCode() {
        int result = first.hashCode();
        result = 31 * result + second.hashCode();
        return result;
    }
}
