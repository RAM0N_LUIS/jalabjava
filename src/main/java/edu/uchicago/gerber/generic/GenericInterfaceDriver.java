package edu.uchicago.gerber.generic;

/**
 * Created by Adam on 7/7/2016.
 */
public class GenericInterfaceDriver {

    public static void main(String[] args) {
        System.out.println((ServiceI<String, Integer>) numbers -> {
            long sum = 0;
            for (Integer num : numbers) {
                sum += num;
            }
            return String.valueOf("The sum is " +sum);
        });





    }
}
