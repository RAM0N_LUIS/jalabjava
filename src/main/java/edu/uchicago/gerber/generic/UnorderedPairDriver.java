package edu.uchicago.gerber.generic;

/**
 * Created by Adam on 7/7/2016.
 */
public class UnorderedPairDriver {
    public static void main(String[] args) {
        UnorderdPair<String> unorderdPair = new UnorderdPair<>("Adam", "Gerber");
        UnorderdPair<String> unorderdPairBackwards = new UnorderdPair<>("Gerber", "Adam");

        System.out.println(unorderdPair.equals(unorderdPairBackwards));
    }
}
