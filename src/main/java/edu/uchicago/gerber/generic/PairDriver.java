package edu.uchicago.gerber.generic;

/**
 * Created by Adam on 7/6/2016.
 */
public class PairDriver {


    public static void main(String[] args) {
        Pair<Integer> parString = new Pair<>();
        Pair<Integer> parString2 = new Pair<>(123, 456);
        parString.setFirst(123);
        parString.setSecond(456);

        System.out.println(parString.equals(parString2));
    }
}
