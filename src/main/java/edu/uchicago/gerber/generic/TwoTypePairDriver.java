package edu.uchicago.gerber.generic;

import java.util.Date;

/**
 * Created by Adam on 7/6/2016.
 */
public class TwoTypePairDriver {

    public static void main(String[] args) {
        TwoTypePair<Date, Long> twoTypePair = new TwoTypePair<>(new Date(464564564564564L),64564645645646L );
        TwoTypePair<Date, Long> twoTypePair2 = new TwoTypePair<>(new Date(464564564564564L),64564645645646L );

        System.out.println(twoTypePair.equals(twoTypePair2));

    }
}
