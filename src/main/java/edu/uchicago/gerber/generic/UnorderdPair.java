package edu.uchicago.gerber.generic;

/**
 * Created by Adam on 7/7/2016.
 */
public class UnorderdPair<T> extends Pair<T> {


    public UnorderdPair() {
        super();
    }

    public UnorderdPair(T first, T second) {
        super(first, second);
    }


    @Override
    public boolean equals(Object o) {

        //todo create this method and a driver.

        if (o == null) {
            return false;
        } else if (getClass() != o.getClass()) {
            return false;
        } else {
            Pair<T> otherPair = (Pair<T>) o;

            if (getFirst().equals(otherPair.getFirst()) && getSecond().equals(otherPair.getSecond()) ||
                    getFirst().equals(otherPair.getSecond()) && getSecond().equals(otherPair.getFirst())
                    ) {
                return true;
            } else {
                return false;
            }


        }

    }
}
