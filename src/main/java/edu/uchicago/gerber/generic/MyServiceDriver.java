package edu.uchicago.gerber.generic;

/**
 * Created by Adam on 7/7/2016.
 */
public class MyServiceDriver {

    public static void main(String[] args) {

      ServiceI<String,Integer> serviceI =  new ServiceI<String, Integer>() {
            @Override
            public String executeService(Integer... args) {
                long sum = 0;
                for (Integer arg : args) {
                    sum += arg;
                }
                return String.valueOf("The sum is " +sum);
            }
        };

        System.out.println(serviceI.executeService(45, 78, 5, 1, 1, 546545454, 5));




    }

}
