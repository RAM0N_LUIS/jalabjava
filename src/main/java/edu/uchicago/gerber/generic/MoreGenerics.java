package edu.uchicago.gerber.generic;

import horstcore.ch11.sec04.Rectangle;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Adam on 7/6/2016.
 */
public class MoreGenerics {

    //lower bounds
    public static void doSomething(List<? super Double> list){

        for (Object obj : list) {
            System.out.println(obj.toString());
        }
    }

    public static void doSomethingAgain(List<? extends Number> list){

        for (Number number : list) {
            System.out.println(number.toString());
        }
    }

    public static void doSomethingAgainSuper(List<? super Double> list){

        for (Object rec : list) {
            System.out.println(rec.toString());
        }
    }


    //just return a value that's passed in
    public static <T> T returnSomething(T t){
        return t;
    }

    public static <T extends Number> List<Double> retunrDoubleList(List<T> list){

        List<Double> doubleList = new ArrayList<>();

        for (T t : list) {
            doubleList.add(t.doubleValue());
        }

        return doubleList;

    }



    public static <R extends Runnable> void runSomething(R r){
        new Thread(r).start();
    }

    public static <C extends Number & Comparable & Serializable> void printSomething(C c){
        System.out.println(c.byteValue());

    }


}
