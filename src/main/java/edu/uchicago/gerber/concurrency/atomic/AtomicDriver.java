package edu.uchicago.gerber.concurrency.atomic;

import java.util.concurrent.atomic.AtomicLong;

public class AtomicDriver {
    AtomicLong al = new AtomicLong(10);


    class AddThread implements Runnable {

        @Override
        public void run() {
            //ads the 5 in current value
            al.addAndGet(5);

            //current value is 15, then 5 is added to current value
            al.compareAndSet(15, 5);

            System.out.println(al.get());
        }

    }

    class SubThread implements Runnable {

        @Override
        public void run() {

            //decrements by 1 from current value
            al.decrementAndGet();


            //weakly compare the first args to current value and sets
            //will not execute because it'll either be 4, 5, 9, 14 or 15 by the time is checks this
            al.weakCompareAndSet(10, 20);
            System.out.println(al.get());
        }

    }

    public static void main(String... args) {
        new Thread(new AtomicDriver().new AddThread()).start();
        new Thread(new AtomicDriver().new SubThread()).start();
    }

}