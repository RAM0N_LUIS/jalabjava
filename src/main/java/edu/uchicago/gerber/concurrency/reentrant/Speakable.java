package edu.uchicago.gerber.concurrency.reentrant;

import java.awt.*;

public interface Speakable {

  String speak();


} //end Speakable
