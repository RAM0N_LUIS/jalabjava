package edu.uchicago.gerber.concurrency.reentrant;

/**
 * Created by ag on 6/17/2015.
 */


public class CollisionOp {

    //this could also be a boolean, but we want to be explicit about what we're doing
    public enum Operation {
        ADD, REMOVE
    }

    //members
    private Speakable mMovable;
    private Operation mOperation;

    //constructor
    public CollisionOp(Speakable movable, Operation op) {
        mMovable = movable;
        mOperation = op;
    }


    //getters
    public Speakable getSpeakable() {
        return mMovable;
    }

    public Operation getOperation() {
        return mOperation;
    }

}
