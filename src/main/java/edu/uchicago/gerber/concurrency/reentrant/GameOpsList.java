package edu.uchicago.gerber.concurrency.reentrant;

import java.util.LinkedList;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by ag on 6/17/2015.
 */
public class GameOpsList extends LinkedList {

    private ReentrantLock lock;

    public GameOpsList() {
        this.lock =   new ReentrantLock();
    }

    public void enqueue(Speakable spk, CollisionOp.Operation operation) {

       try {
            lock.lock();
            addLast(new CollisionOp(spk, operation));
        } finally {
            lock.unlock();
        }
    }

    public CollisionOp dequeue() {
        try {
            lock.lock();
           return (CollisionOp) super.removeFirst();
        } finally {
            lock.unlock();
        }
    }
}
