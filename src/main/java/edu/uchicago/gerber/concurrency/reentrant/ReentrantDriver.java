package edu.uchicago.gerber.concurrency.reentrant;

/**
 * Created by Adam on 7/13/2016.
 */
public class ReentrantDriver {

    public static void main(String[] args) {

        GameOpsList gameOpsList = new GameOpsList();



        Runnable producer1 = new Runnable() {
            @Override
            public void run() {

                for (int nC = 0; nC < 100; nC++) {
                    gameOpsList.enqueue(new Speakable() {
                        @Override
                        public String speak() {
                            return "Yo Rocky";
                        }
                    }, CollisionOp.Operation.ADD);
                }


            }
        };


        Runnable producer2 = new Runnable() {
            @Override
            public void run() {

                for (int nC = 0; nC < 100; nC++) {
                    gameOpsList.enqueue(new Speakable() {
                        @Override
                        public String speak() {
                            return "Yo Adrian";
                        }
                    }, CollisionOp.Operation.ADD);
                }


            }
        };


        Runnable consumer = new Runnable() {
            @Override
            public void run() {

                int nC = 1;
                while (true){
                    if (gameOpsList.isEmpty()){
                        break;
                    }
                    System.out.println(((CollisionOp)gameOpsList.dequeue()).getSpeakable().speak() + nC++);
                }
                System.out.println("%%%%%%%%%%%%%%%%%%%%%%%%%%%% finish");
            }
        };

        new Thread(producer1).start();
        new Thread(producer2).start();

        try {
            Thread.sleep(20);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        new Thread(consumer).start();


    }
}
