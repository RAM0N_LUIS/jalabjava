package edu.uchicago.gerber.concurrency.cyclic;

import java.util.concurrent.CyclicBarrier;

public class CyclicDriver {


    public static void main(String[] args) {


        CyclicBarrier barrier = new CyclicBarrier(3, new Runnable() {
            private int count = 1;

            @Override
            public void run() {
                System.out.println("Completed..!! " + (count++));
            }
        });

        Thread t1 = new Thread(new CyclicRunner(barrier));
        t1.start();

        Thread t2 = new Thread(new CyclicRunner(barrier));
        t2.start();


        Thread t3 = new Thread(new CyclicRunner(barrier));
        t3.start();


    }
}