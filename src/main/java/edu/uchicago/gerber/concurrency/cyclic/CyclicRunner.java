package edu.uchicago.gerber.concurrency.cyclic;

import java.util.Random;
import java.util.concurrent.CyclicBarrier;

public class CyclicRunner implements Runnable {

    private CyclicBarrier barrier;
    private static Random sRandom = new Random();

    public CyclicRunner(CyclicBarrier barrier) {
        this.barrier = barrier;

    }

    @Override
    public void run() {
        try {

            Thread.sleep(sRandom.nextInt(5000));

            System.out.println(Thread.currentThread().getName() + " :: Waiting At Barrier 1 After Stage 1 Completed");
            barrier.await();

            Thread.sleep(sRandom.nextInt(5000));
            System.out.println(Thread.currentThread().getName() + " :: Waiting At Barrier 2 After Stage 2 Completed");
            barrier.await();

            Thread.sleep(sRandom.nextInt(5000));
            System.out.println(Thread.currentThread().getName() + " :: Waiting At Barrier 3 After Stage 3 Completed");
            barrier.await();
            System.out.println(Thread.currentThread().getName() + " :: $$$$$$$$ Completed $$$$$$$$");
        } catch (Exception ex) {
            ex.printStackTrace();
        }


    }
}