package edu.uchicago.gerber.concurrency.countdown;

import java.util.concurrent.CountDownLatch;

/**
 * Created by Adam on 7/14/2016.
 */
public class ServiceTwo implements Runnable{

    private final CountDownLatch latch;
    public ServiceTwo(CountDownLatch latch) {

        this.latch = latch;

    }

    @Override
    public void run() {
        System.out.println("Started service Two");

        //reduce count of Count Down Latch by 1.

        latch.countDown();
    }
}
