package edu.uchicago.gerber.concurrency.semaphore;

import java.util.concurrent.Semaphore;

/**
 * Created by Adam on 7/14/2016.
 */
public class SemaphoreDriver {

    //note: you should not throw an exception off main!
    public static void main(String[] args) throws InterruptedException {

        int i = 0;
        Semaphore sem = new Semaphore(1); // 1 permission available
        sem.release();                    // 2 permissions available after this method
        System.out.println(i++); // 1 then ++
        sem.release();                    // 3 permissions available after this method
        System.out.println(i++); // 2
        sem.acquire();                    // 2 permissions available after this method
        System.out.println(i++); // 3
        sem.acquire();                    // 1 permission available after this method
        System.out.println(i++); // 4
        sem.acquire();                    // 0 permissions available after this method
        System.out.println(i++); // 5

        sem.acquire();                    // now thread needs to wait for available permission
        System.out.println(i++); // 6
        sem.acquire();
        System.out.println(i++); // 7
    }
}
