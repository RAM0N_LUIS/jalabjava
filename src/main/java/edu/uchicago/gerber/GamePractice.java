package edu.uchicago.gerber;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Bounds;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.awt.*;
import java.util.ArrayList;
import java.util.Random;

//modifed but taken originally from
//http://stackoverflow.com/questions/20022889/how-to-make-the-ball-bounce-off-the-walls-in-javafx
public class GamePractice extends Application {


    public static ArrayList<MyCircle> redCs = new ArrayList<>();
    public static ArrayList<MyCircle> blueCs = new ArrayList<>();
    public static Pane canvas;

    @Override
    public void start(final Stage primaryStage) {

        canvas = new Pane();
        final Scene scene = new Scene(canvas, 800, 600);

        primaryStage.setTitle("Bouncing Balls");
        primaryStage.setScene(scene);
        primaryStage.show();


        redCs.add(new MyCircle(Color.RED, new Point(100, 150)));
        redCs.add(new MyCircle(Color.RED, new Point(100, 300)));
        redCs.add(new MyCircle(Color.RED, new Point(100, 350)));

        blueCs.add(new MyCircle(Color.BLUE, new Point(100, 150)));
        blueCs.add(new MyCircle(Color.BLUE, new Point(100, 300)));
        blueCs.add(new MyCircle(Color.BLUE, new Point(100, 350)));

        for (MyCircle cir : redCs) {
            canvas.getChildren().addAll(cir);
        }

        for (MyCircle cir : blueCs) {
            canvas.getChildren().addAll(cir);
        }



        final Timeline loopRed = new Timeline(new KeyFrame(Duration.millis(10), new EventHandler<ActionEvent>() {

            @Override
            public void handle(final ActionEvent t) {

                for (MyCircle cir : redCs) {



                    cir.setLayoutX(cir.getLayoutX() + cir.getDeltaX());
                    cir.setLayoutY(cir.getLayoutY() + cir.getDeltaY());

                    final Bounds bounds = canvas.getBoundsInLocal();
                    final boolean atRightBorder = cir.getLayoutX() >= (bounds.getMaxX() - cir.getRadius());
                    final boolean atLeftBorder = cir.getLayoutX() <= (bounds.getMinX() + cir.getRadius());
                    final boolean atBottomBorder = cir.getLayoutY() >= (bounds.getMaxY() - cir.getRadius());
                    final boolean atTopBorder = cir.getLayoutY() <= (bounds.getMinY() + cir.getRadius());

                    if (atRightBorder || atLeftBorder) {
                        cir.setDeltaX(cir.getDeltaX() * -1);

                    }
                    if (atBottomBorder || atTopBorder) {
                        cir.setDeltaY(cir.getDeltaY() * -1);
                    }

                }
            }
        }));

        final Timeline loopBlue = new Timeline(new KeyFrame(Duration.millis(10), new EventHandler<ActionEvent>() {

            @Override
            public void handle(final ActionEvent t) {

                for (MyCircle cir : blueCs) {



                    cir.setLayoutX(cir.getLayoutX() + cir.getDeltaX());
                    cir.setLayoutY(cir.getLayoutY() + cir.getDeltaY());

                    final Bounds bounds = canvas.getBoundsInLocal();
                    final boolean atRightBorder = cir.getLayoutX() >= (bounds.getMaxX() - cir.getRadius());
                    final boolean atLeftBorder = cir.getLayoutX() <= (bounds.getMinX() + cir.getRadius());
                    final boolean atBottomBorder = cir.getLayoutY() >= (bounds.getMaxY() - cir.getRadius());
                    final boolean atTopBorder = cir.getLayoutY() <= (bounds.getMinY() + cir.getRadius());

                    if (atRightBorder || atLeftBorder) {
                        cir.setDeltaX(cir.getDeltaX() * -1);

                    }
                    if (atBottomBorder || atTopBorder) {
                        cir.setDeltaY(cir.getDeltaY() * -1);
                    }

                }
            }
        }));

        loopRed.setCycleCount(Timeline.INDEFINITE);
        loopRed.play();

        loopBlue.setCycleCount(Timeline.INDEFINITE);
        loopBlue.play();
    }

    public static void main(final String[] args) {
        launch(args);
    }


    class MyCircle extends Circle {

        private double deltaY;
        private double deltaX;



         public MyCircle(Color color, Point point) {

             super(new Random().nextInt(20) + 5
                     , color);
             relocate(point.x, point.y);


             setDeltaX(new Random().nextInt(10));
             setDeltaY(new Random().nextInt(10));
        }

        public double getDeltaY() {
            return deltaY;
        }

        public void setDeltaY(double deltaY) {
            this.deltaY = deltaY;
        }

        public double getDeltaX() {
            return deltaX;
        }

        public void setDeltaX(double deltaX) {
            this.deltaX = deltaX;
        }


    }
}